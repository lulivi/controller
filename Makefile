
default: publish

build:
	poetry build

publish: build
	scp dist/controller-0.0.1-py3-none-any.whl vps:/home/exec/apps/controller/
