# Copyright (c) 2024 Luis Liñán
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import logging

from typing import Final, List

from telegram.constants import UpdateType
from telegram.ext import Application, CommandHandler, MessageHandler, filters

from controller.bot import cmd, cmd_cd, cmd_help
from controller.settings import ALLOWED_CHATS, BOT_TOKEN

logging.basicConfig(
    format="{asctime}|{levelname}|{name}:{lineno: 4d}| {message}",
    style="{",
    level=logging.DEBUG,
    datefmt="%Y/%m/%d-%H:%M",
)
logging.getLogger("httpx").setLevel(logging.WARNING)
logging.getLogger("httpcore").setLevel(logging.WARNING)
logging.getLogger("telegram").setLevel(logging.INFO)
logging.getLogger("asyncio").setLevel(logging.INFO)

logger = logging.getLogger(__name__)


def main() -> None:
    """Start the bot."""
    logger.info("***************************")
    logger.info("* Starting controller bot *")
    logger.info("***************************")
    application = Application.builder().token(BOT_TOKEN).build()
    app_filters: Final[List[filters.BaseFilter]] = filters.TEXT & filters.Chat(ALLOWED_CHATS)
    application.add_handler(CommandHandler(command="help", filters=app_filters, callback=cmd_help))
    application.add_handler(CommandHandler(command="cd", filters=app_filters, callback=cmd_cd))
    application.add_handler(MessageHandler(filters=app_filters, callback=cmd))
    application.run_polling(allowed_updates=UpdateType.MESSAGE)


if __name__ == "__main__":
    main()
