# Copyright (c) 2024 Luis Liñán
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import html
import json
import logging
import subprocess
import traceback

from pathlib import Path

from telegram import Update
from telegram.constants import ParseMode
from telegram.ext import ContextTypes

from controller.settings import HOME_PATH
from controller.utils import _find_correct_path

logger = logging.getLogger(__name__)


async def cmd_cd(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    if update.message is None or update.message.text is None:
        return

    previous_path: Path = context.chat_data.get("cwd") or HOME_PATH
    selected_path = update.message.text.replace("/cd", "").strip()
    logger.debug(f"Previous working directory: `{previous_path}`")
    logger.debug(f"Prompted: `cd {selected_path}`")
    corrected_path = _find_correct_path(selected_path, previous_path)
    context.chat_data["cwd"] = corrected_path
    logger.debug(f"Selecting working directory: `{corrected_path}`")
    await update.message.reply_text(
        f"<pre>New CWD: {str(corrected_path)}</pre>",
        parse_mode=ParseMode.HTML,
    )


async def cmd(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    if update.message is None or update.message.text is None:
        return

    command = update.message.text

    logger.debug(f"Running command `{command}`")
    completed_process: subprocess.CompletedProcess = subprocess.run(
        args=[command],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        text=True,
        shell=True,
        cwd=context.chat_data.get("cwd") or HOME_PATH,
    )

    buffer: str = f"{completed_process.stdout.strip()}\n"
    message_end_index: int = 0

    while buffer and buffer != "\n":
        try:
            message_end_index = buffer.rindex("\n", 0, 4090)
        except ValueError:
            message_end_index = 4090

        message_to_send, buffer = buffer[:message_end_index], buffer[message_end_index:]
        await update.message.reply_text(
            f"<pre>{html.escape(message_to_send)}</pre>",
            parse_mode=ParseMode.HTML,
        )


async def cmd_help(update: Update, _: ContextTypes.DEFAULT_TYPE) -> None:
    await update.message.reply_text(
        "Current configured commands:\n\n- <code>/cd</code>: Change permantly the current working"
        " directory. If no directory is provided after the command, the home directory will be"
        " selected.\n\n- Any text message without command will be executed and the output"
        " returned. E.g.: <code>ls -la</code>",
        parse_mode=ParseMode.HTML,
    )


async def error(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    if context.error is None:
        logger.error("Exception while handling an update. Counld not get any error information")
        return

    logger.error("Exception while handling an update:", exc_info=context.error)
    traceback_str = html.escape(
        "".join(
            traceback.format_exception(
                None,
                context.error,
                context.error.__traceback__,
            )
        )
    )
    update_str = html.escape(
        json.dumps(
            update.to_dict() if isinstance(update, Update) else str(update),
            indent=2,
            ensure_ascii=False,
        )
    )
    update_message = f"<pre>update = {update_str}</pre>"
    error_message = f"<pre>{html.escape(traceback_str)}</pre>"
    await context.bot.send_message(
        chat_id=update.message.chat_id,
        text="<code>************************************************</code>",
        parse_mode=ParseMode.HTML,
    )
    await context.bot.send_message(
        chat_id=update.message.chat_id,
        text="An exception was raised while handling an update",
    )
    await context.bot.send_message(
        chat_id=update.message.chat_id,
        text=update_message,
        parse_mode=ParseMode.HTML,
    )
    await context.bot.send_message(
        chat_id=update.message.chat_id,
        text=error_message,
        parse_mode=ParseMode.HTML,
    )
    await context.bot.send_message(
        chat_id=update.message.chat_id,
        text="<code>************************************************</code>",
        parse_mode=ParseMode.HTML,
    )
