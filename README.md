# Controller bot

> Run commands in your server through telegram

## Set-up

1.  Move `example.env` to `.env` and populate the proper variables. E.g.::

    ```ini
    CONTROLLER_BOT_TOKEN = 12345:abcdef
    CONTROLLER_ALLOWED_CHATS = 12345,56789
    ```

2. Create a virtual environment and install the package with `poetry`.
3. Run `controller` and use it!

## License

This projects is published under the [MIT license](./LICENSE)
